#!/usr/bin/env python
# encoding: utf-8

import sentiplp
import nltk.tree

def assert_eq(a, b):
    assert a == b

tree = lambda sexpr: \
        nltk.tree.Tree.parse(sexpr, parse_leaf=lambda ln: tuple(ln.split("/")))

def tag_test_generator():
    tagger = sentiplp.PLPUnigramTagger()
    expected = [("kot", "RZ"), ("on", "ZA"), ("jeść", "CZ")]
    yield (assert_eq, expected, tagger.tag(["kot", "on", "jeść"]))
    yield (assert_eq,
            [tag for (_,tag) in expected],
            [tag for (_,tag) in tagger.tag(["kot", "go", "je"])])

def adjective_search_test():
    from os.path import dirname, join
    testfiles = [join(dirname(__file__), "..", "data", file)
            for file in ["pap-1k.txt", "rzadki.txt"]]
    as_ = sentiplp.AdjectiveSearch(*testfiles)
    as_.run()
    text = u'''rzadki, 100
               drugi, 13
               europejski, 13
               amerykański, 12
               polski, 10
               jeden, 10
               światowy, 9
               pierwszy, 8
               prasowy, 8
               trzeci, 8
               wielki, 8
               ostatni, 7
               nowy, 7
               cały, 7
               zachodni, 6
               największy, 6
               polityczny, 6
               północny, 6
               podstawowy, 6
               międzynarodowy, 6'''
    #print as_.pretty_results()
    expected = list(ln.strip().split(", ", 2)[0] for ln in text.split("\n"))
    assert_eq(expected, as_.results)

def conjunction_tagger_test_generator():
    ct = sentiplp.ConjunctionTagger(sentiplp.PLPUnigramTagger())
    input = [[u"ładny", ",", "ale", u"głupi"],
             ["cham", "i", "prostak"],
             [u"rozsądny", u"tudzież", "roztropny"]]
    expected = [[(u"ładny","PM"), (",","IP"), ("ale","PRZECIWSTAWNY"),
                 (u"głupi","PM")],
                [("cham","RZ"), ("i","LACZNY"), (u"prostak","RZ")],
                [(u"rozsądny", "PM"), (u"tudzież", "LACZNY"),
                 ("roztropny", "PM")]]
    tagset = ["LACZNY",
              "ROZLACZNY",
              "WYKLUCZAJACY",
              "PRZECIWSTAWNY",
              "WYJASNIAJACY",
              "WYNIKOWY"]
    for e,i in zip(expected, input):
        yield (assert_eq, e, ct.tag(i))
    yield (assert_eq, sorted(tagset), sorted(ct.tagset))

def conjunction_parser_test_generator():
    at = sentiplp.AdjectiveTagger(sentiplp.PLPUnigramTagger())
    ct = sentiplp.ConjunctionTagger(at)
    cp = sentiplp.ConjunctionParser()
    input = [["ani", "zdolny", "ani", "pracowity"],
             ["cwany", ",", "ale", "chytry"]]
    input.extend([input[0] + input[1]])
    expected = [
        tree("""(S ani/WYKLUCZAJACY
                   (SIMILAR zdolny/PM ani/WYKLUCZAJACY pracowity/PM))"""),
        tree("""(S (INVERSE cwany/PM ,/IP ale/PRZECIWSTAWNY chytry/PM))"""),
        tree("""(S ani/WYKLUCZAJACY
                   (SIMILAR zdolny/PM ani/WYKLUCZAJACY pracowity/PM)
                   (INVERSE cwany/PM ,/IP ale/PRZECIWSTAWNY chytry/PM))""")
    ]
    for e,i in zip(expected, input):
        yield (assert_eq, e, cp.parse(ct.tag(i)))

def opinion_word_finder_test_generator():
    wf = sentiplp.OpinionWordFinder()
    input = [
        tree("""(S ani/WYKLUCZAJACY
                   (SIMILAR zdolny/PM ani/WYKLUCZAJACY pracowity/PM)
                   (INVERSE cwany/PM ,/IP ale/PRZECIWSTAWNY chytry/PM))""")
    ]
    expected = [
        [("SIMILAR", ("zdolny","PM"), ("pracowity","PM")),
         ("INVERSE", ("cwany","PM"), ("chytry","PM"))]
    ]
    for e,i in zip(expected, input):
        yield (assert_eq, e, wf.find(i))

def orientation_finder_test():
    initial_opinions = [("zdolny", 1.0), ("dobry", 1.0), ("niedobry", -1.0)]
    opinion_words = [("INVERSE", "zdolny", "leniwy"),
                     ("SIMILAR", "niedobry", "leniwy")]
    of = sentiplp.OrientationFinder(initial_opinions, opinion_words)
    of.run()
    #sentiplp.graph.show(of.graph)
    sentiplp.graph.show_partition(of.graph, of.partition)
