console:
	PYTHONPATH="$(shell pwd)/sentiplp" ipython

.PHONY: test
test:
	@nosetests --nocapture test/*.py
