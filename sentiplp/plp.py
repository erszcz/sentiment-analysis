#!/usr/bin/env python
# encoding: utf-8

import os.path
from ctypes import *

CLPLIBPATH = os.path.join(os.path.dirname(__file__), "lib", "libclp.so")
CLPLIB = CDLL(CLPLIBPATH)
CLPLIB.clp_init(1)

def ver():
	"""Zwraca napis z numerem wersji CLP"""
	ver = create_string_buffer(80)
	CLPLIB.clp_ver(ver)
	return 'P' + ver.value[1:]

def stat(pos):
	"""Zwraca statystyke czesci mowy w bibliotece CLP"""
	return CLPLIB.clp_stat(c_int(pos))

def pos(id):
	"""Zwraca numer czesci mowy dla danego ID"""
	return CLPLIB.clp_pos(c_int(id))

def label(id):
	"""Zwraca etykiete dla danego ID"""
	label = create_string_buffer(10)
	CLPLIB.clp_label(c_int(id), label)
	return label.value.decode("utf-8")

def bform(id):
	"""Zwraca forme podstawowa dla danego ID"""
	bform = create_string_buffer(80)
	CLPLIB.clp_bform(c_int(id), bform)
	return bform.value.decode("utf-8")

def forms(id):
	"""Zwraca liste form dla danego wyrazu"""
	forms_ = create_string_buffer(2048)
	CLPLIB.clp_forms(c_int(id), forms_)
	return [f.decode("utf-8") for f in forms_.value.split(':')[0:-1]]

def formv(id):
	"""Zwraca wektor form dla danego wyrazu"""
	vector = create_string_buffer(2048)
	CLPLIB.clp_formv(c_int(id), vector)
	return [f.decode("utf-8") for f in vector.value.split(':')[0:-1]]

def vec(id, word):
	"""Zwraca wektor odmiany"""
	Array50 = c_int * 50
	out = Array50()
	num = c_int(0)
	if type(word) == unicode:
		word = word.encode("utf-8")
	CLPLIB.clp_vec(c_int(id), word, out, byref(num))
	return out[0:num.value]

def rec(word):
	"""Zwraca liste numerow ID dla danego slowa"""
	Array50 = c_int * 50
	ids = Array50()
	num = c_int(0)
	if type(word) == unicode:
		word = word.encode("utf-8")
	CLPLIB.clp_rec(word, ids, byref(num))
	return ids[0:num.value]

## requires lp1c database
def pid(id):
	return CLPLIB.clp_pid(c_int(id))
