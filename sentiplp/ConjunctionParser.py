#!/usr/bin/env python
# encoding: utf-8

# Conjunctions between adjectives:
# - "fair and legitimate"
# - "corrupt and brutal"

import nltk

class ConjunctionParser(object):

    rules = r'''
        SIMILAR:    { <PM|PW> <IP>? <LACZNY> <PM|PW> }
                    { <PM|PW> <IP>? <WYKLUCZAJACY> <PM|PW> }
                    { <PM|PW> <IP>? <WYJASNIAJACY> <PM|PW> }
                    { <PM|PW> <IP>? <WYNIKOWY> <PM|PW> }
        INVERSE:    { <PM|PW> <IP>? <ROZLACZNY> <PM|PW> }
                    { <PM|PW> <IP>? <PRZECIWSTAWNY> <PM|PW> }
    '''

    def __init__(self):
        self.parser = nltk.RegexpParser(self.rules)

    def parse(self, tagged_tokens):
        return self.parser.parse(tagged_tokens)
