#!/usr/bin/env python
# encoding: utf-8

import nltk
import plp

class AdjectiveTagger(nltk.tag.SequentialBackoffTagger):

    translate = True
    return_bforms = False

    def __init__(self, backoff=None):
        nltk.tag.SequentialBackoffTagger.__init__(self, backoff)

    def tag(self, tokens):
        def tag_one(token):
            ids = plp.rec(token)
            for id in ids:
                label = plp.label(id)
                tag = self.__translate(label)
                if tag in ['PM', 'PW']:
                    return (plp.bform(id) if self.return_bforms else token,
                            label if not self.translate else tag)
            return (token, None)
        return [tag_one(token) for token in tokens]

    def choose_tag(self, tokens, index, history):
        return self.tag([tokens[index]])[0][1]

    def __translate(self, label):
        return {'A': 'RZ',  # rzeczownik
                'B': 'CZ',  # czasownik
                'C': 'PM',  # przymiotnik
                'D': 'LI',  # liczebnik
                'E': 'ZA',  # zaimek
                'F': 'PW',  # przysłówek
                'G': 'ND',  # nieodmienny
                'H': 'SE',  # segment
                'I': 'SK',  # skrót
                'X': 'IP'   # znak interpunkcyjny
               }[label[0]]
