#!/usr/bin/env python
# encoding: utf-8

class OpinionWordFinder(object):

    classes = set(["SIMILAR", "INVERSE"])

    def find(self, tree):
        opinion_words = []
        next_ = [tree]
        # is adjective or adverb?
        is_adjv = lambda (_,tag): tag in ["PM", "PW"]
        while next_:
            node = next_.pop(0)
            next_.extend(list(node.subtrees())[1:])
            if node.node not in self.classes:
                continue
            opinion_words.append(
                    tuple([node.node] + filter(is_adjv, node.leaves())))
        return opinion_words
