#!/usr/bin/env python
# encoding: utf-8

import nltk

class ConjunctionTagger(nltk.tag.RegexpTagger):

    # łączne, np. a, i, oraz, tudzież
    # rozłączne, np. albo, bądź, czy, lub
    # wykluczające, np. ani, ni
    # przeciwstawne, np. a, aczkolwiek, ale, jednak, lecz, natomiast, zaś
    # wyjaśniające, np. czyli, mianowicie
    # wynikowe, np. dlatego, i, przeto, tedy, więc, zatem, toteż
    #
    # Źródło:
    # http://pl.wikipedia.org/wiki/Sp%C3%B3jnik_%28cz%C4%99%C5%9B%C4%87_mowy%29

    conjunctions = {
        # "a" is ambiguous, hence not included
        "LACZNY"       : ["i", "oraz", u"tudzież"],
        "ROZLACZNY"    : ["albo", u"bądź", "czy", "lub"],
        "WYKLUCZAJACY" : ["ani", "ni"],
        # again, "a" is ambiguous
        "PRZECIWSTAWNY": ["aczkolwiek", "ale", "jednak", "lecz",
                          "natomiast", u"zaś"],
        "WYJASNIAJACY" : ["czyli", "mianowicie"],
        # "i" is already used as "laczny"
        "WYNIKOWY"     : ["dlatego", "przeto", "tedy", u"więc", "zatem",
                          u"toteż"]
    }

    def __init__(self, backoff=None):
        regexps = []
        for k,vs in self.conjunctions.items():
            regexps.extend((v,k) for v in vs)
        nltk.tag.RegexpTagger.__init__(self, regexps, backoff=backoff)

    @property
    def tagset(self):
        return self.conjunctions.keys()
