#!/usr/bin/env python
# encoding: utf-8

import networkx as nx
import pylab

def show(graph):
    pos = nx.circular_layout(graph)
    nx.draw(graph, pos, font_size=16, with_labels=False)
    # raise text positions
    for p in pos:
        pos[p][1] += 0.07
    labels = dict((n,"%s (%s)" % (n,d['opinion']))
            for n,d in graph.nodes(data=True))
    nx.draw_networkx_labels(graph, pos, labels)
    nx.draw_networkx_edge_labels(graph, pos)
    pylab.show()

def show_partition(graph, partition):
    size = float(len(set(partition.values())))
    pos = nx.circular_layout(graph)
    for p in pos:
        pos[p][1] += 0.07
    count = 0.0
    for com in set(partition.values()) :
        count = count + 1.0
        list_nodes = [nodes for nodes in partition.keys()
                                    if partition[nodes] == com]
        nx.draw_networkx_nodes(graph, pos, list_nodes,
                                node_color = str(count / size))
    labels = dict((n,"%s (%s)" % (n,d['opinion']))
            for n,d in graph.nodes(data=True))
    nx.draw_networkx_labels(graph, pos, labels)
    nx.draw_networkx_edges(graph, pos)
    nx.draw_networkx_edge_labels(graph, pos)
    pylab.show()
