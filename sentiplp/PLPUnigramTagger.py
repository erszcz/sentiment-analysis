#!/usr/bin/env python
# encoding: utf-8

import nltk
import plp

class PLPUnigramTagger(nltk.tag.SequentialBackoffTagger):

    translate = True
    return_bforms = False

    def __init__(self, backoff=None):
        nltk.tag.SequentialBackoffTagger.__init__(self, backoff)

    def tag(self, tokens):
        def map(id):
            label = plp.label(id)
            return plp.bform(id) if self.return_bforms else token, \
                label if not self.translate else self.__translate(label)
        return [self.__select(token, map) for token in tokens]

    def choose_tag(self, tokens, index, history):
        return self.tag([tokens[index]])[0][1]

    def __select(self, token, map):
        ids = plp.rec(token)
        if ids:
            return map(ids[0])
        else:
            return token, None

    def __translate(self, label):
        return {'A': 'RZ',  # rzeczownik
                'B': 'CZ',  # czasownik
                'C': 'PM',  # przymiotnik
                'D': 'LI',  # liczebnik
                'E': 'ZA',  # zaimek
                'F': 'PW',  # przysłówek
                'G': 'ND',  # nieodmienny
                'H': 'SE',  # segment
                'I': 'SK',  # skrót
                'X': 'IP'   # znak interpunkcyjny
               }[label[0]]
