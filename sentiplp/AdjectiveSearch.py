#!/usr/bin/env python
# encoding: utf-8

import itertools as it
import nltk
import plp

from PLPUnigramTagger import PLPUnigramTagger

class AdjectiveSearch(object):

    limit = 20

    def __init__(self, *args):
        self.filenames = list(args)
        self.histogram = {}
        self.results = []
        self.hook = None

    @property
    def histogram(self):
        return self._histogram

    @histogram.setter
    def histogram(self, dict_):
        self._histogram = Histogram(dict_)

    def run(self):
        for filename in self.filenames:
            self.search_file(filename)
        self.results = list(reversed(sorted(self.histogram,
                key=lambda k: self.histogram[k])[-self.limit:]))

    def search_file(self, filename):
        h = self.histogram
        tagger = PLPUnigramTagger()
        tagger.return_bforms = True
        tokenize = nltk.wordpunct_tokenize
        with file(filename, 'r') as f:
            for line in f.readlines():
                sentences = nltk.sent_tokenize(line.decode("utf-8"))
                tagged_sentences = tagger.batch_tag(tokenize(sent)
                        for sent in sentences)
                tagged_bforms = it.chain.from_iterable(tagged_sentences)
                for (bform, tag) in tagged_bforms:
                    if tag == "PM" or tag == "PW":
                        h[bform] += 1
                    else:
                        continue
        if self.hook:
            self.hook(self, filename)

    def pretty_results(self):
        return "\n".join("%s, %d" % (k,self.histogram[k])
                for k in self.results)


class Histogram(dict):

    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)

    def __getitem__(self, key):
        if dict.has_key(self, key):
            return dict.__getitem__(self, key)
        else:
            return 0

    def __setitem__(self, key, val):
        dict.__setitem__(self, key, val)

    def __repr__(self):
        return '%s(%s)' % (type(self).__name__, dict.__repr__(self))

    def update(self, *args, **kwargs):
        for k, v in dict(*args, **kwargs).iteritems():
            self[k] = v
