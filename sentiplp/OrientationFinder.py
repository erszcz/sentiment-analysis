#!/usr/bin/env python
# encoding: utf-8

import community
import graph
import networkx as nx

class OrientationFinder(object):

    def __init__(self, initial_opinions, opinion_words):
        self.initial_opinions = initial_opinions
        self.opinion_words = opinion_words
        self.graph = nx.Graph()
        self.partition = None

    def run(self):
        self.build_graph()
        self.partition = community.best_partition(self.graph)

    def build_graph(self):
        g = self.graph
        for (relation, word1, word2) in self.opinion_words:
            g.add_node(word1, opinion=0.0)
            g.add_node(word2, opinion=0.0)
            if relation == "SIMILAR":
                g.add_edge(word1, word2, dissimilarity=0.0)
            elif relation == "INVERSE":
                g.add_edge(word1, word2, dissimilarity=1.0)
        for (word, op) in self.initial_opinions:
            g.add_node(word, opinion=op)
            #g.add_node(word, opinion=0.0)
        nodes = g.nodes()
        for i in xrange(len(nodes)):
            for j in xrange(i+1, len(nodes)):
                edges = g.edges()
                if ((nodes[i],nodes[j]) not in edges
                        and (nodes[j],nodes[i]) not in edges):
                    g.add_edge(nodes[i], nodes[j], dissimilarity=0.5)
