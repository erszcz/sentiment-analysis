#!/usr/bin/env python
# encoding: utf-8

import os
import sentiplp

from cPickle import dump, load
from datetime import datetime

ADJECTIVES = "adjectives.pickle"
TEXTS = [["pap1.txt"],
         ["rzadki.txt"]]

def main(args):
    if len(args) <= 1:
        usage(args[0])
        exit()
    if   "search" == args[1]:
        search()
    elif "results" == args[1] and len(args) > 2:
        if len(args) > 3:
            display_results(args[2], int(args[3]))
        else:
            display_results(args[2])
    else:
        usage(args[0])

def search():
    if is_in_progress():
        search = restore_adjective_search()
    else:
        search = start_adjective_search()
    search.run()
    finalize_search()

def display_results(filename, limit=None):
    with file(filename, 'rb') as f:
        histogram = load(f)
    if histogram["__meta__"].has_key("texts"):
        print "Źródła:\n  " \
              + "\n  ".join(histogram["__meta__"]["texts"])
    del histogram["__meta__"]
    search = sentiplp.AdjectiveSearch()
    if limit:
        search.limit = limit
    search.histogram = histogram
    search.run()
    print "Wyniki:\n  " \
          + search.pretty_results().replace("\n", "\n  ")

def usage(progname):
    print '''\
usage: %(progname)s search
       %(progname)s results ADJECTIVES

  search    -- search for adjectives in defined corpus
  results   -- display results found in pickled ADJECTIVES histogram\
''' % {"progname": progname}

def is_in_progress():
    return os.path.exists(ADJECTIVES)

def restore_adjective_search():
    with file(ADJECTIVES, 'rb') as f:
        histogram = load(f)
    meta = histogram["__meta__"]
    texts_left = meta["texts_left"]
    search = sentiplp.AdjectiveSearch(texts_left)
    del histogram["__meta__"]
    search.histogram = histogram
    search.hook = mk_checkpoint(meta)
    print "restoring adjective search"
    print meta
    return search

def mk_checkpoint(meta):
    def checkpoint(search, filename):
        histogram = search.histogram
        meta["texts_left"].remove(filename)
        histogram["__meta__"] = meta
        with file(ADJECTIVES, 'wb') as f:
            dump(histogram, f)
        del histogram["__meta__"]
        print "checkpointed:", filename
    return checkpoint

def start_adjective_search():
    texts = initial_corpus()
    meta = {"texts": texts,
            "texts_left": list(texts)}
    search = sentiplp.AdjectiveSearch(*meta["texts_left"])
    search.hook = mk_checkpoint(meta)
    print "starting adjective search"
    print meta
    return search

def initial_corpus():
    data = os.path.join(os.path.dirname(__file__), "data")
    return [os.path.join(data, *text) for text in TEXTS]

def finalize_search():
    now = datetime.isoformat(datetime.now())
    result_file = "adjectives.%s.pickle" % now
    os.rename(ADJECTIVES, result_file)
    print "search done, archived:", result_file

if __name__ == '__main__':
    import sys
    main(sys.argv)
